if [ ! -d "build" ]; then mkdir build; fi
if  ! find build/ -maxdepth 0 -empty | read v ; then  rm -r build/*; fi

cd build
cmake ..
make
./OSM_A_star_search

